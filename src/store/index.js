import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './rootReducer';
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancer = composeEnhancers(
  applyMiddleware(thunk),
);

export const store = process.env.NODE_ENV === 'production' ?
  createStore(rootReducer, applyMiddleware(thunk))
  : createStore(rootReducer, enhancer);
