import { EVENT } from "../../events";

export const AppReducer = (state = { albums: [] }, action) => {
  switch (action.type) {
    case EVENT.LOAD_INITIAL_STATE:
        return {
            ...state,
            albums: action.data
        };
    default:
      return state;
  }
};