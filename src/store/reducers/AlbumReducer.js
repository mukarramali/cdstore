import { EVENT } from "../../events";

export const AlbumReducer = (state = {albums: []}, action) => {
  switch (action.type) {
    case EVENT.UPDATE:
        return state;
    default:
        return state;
  }
};