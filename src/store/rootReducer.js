import { combineReducers } from 'redux';
import { AlbumReducer } from './reducers/AlbumReducer';
import { AppReducer } from './reducers/AppReducer';

const appReducer = combineReducers({
    albumReducer: AlbumReducer,
    appReducer: AppReducer
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
}

export default rootReducer;
