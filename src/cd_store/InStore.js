import React from 'react'
import Album from './Album'

class InStore extends React.Component {


    render() {
        return this.props.albums.map((album, index) => {
            return <Album {...album} key={index}/>
        })
    }
}

export default InStore;