import React from 'react'
import RentedOut from './RentedOut'
import InStore from './InStore'
import { EVENT } from '../events'

import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
  progress: {
    margin: theme.spacing(2),
  },
});

const ALBUMS_API = "https://jsonplaceholder.typicode.com/photos"

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inStore: {
            },
            rentedOut: {
            },
            spinner: true
        };
        this.ifShowSpinner = this.ifShowSpinner.bind(this);
        this.handleResponse = this.handleResponse.bind(this);
    }

    handleResponse(data) {
       this.setState({spinner: false});
       this.props.loadInitialState(data);
    }

    componentDidMount() {
        fetch(ALBUMS_API, {headers: {'Access-Control-Allow-Origin': '*'}})
        .then(response => response.text())
        .then(this.handleResponse)
        .catch(exc => {
            console.log("Couldn't process!", exc);
        });
    }


    ifShowSpinner() {
        return this.state.spinner;
    }

    filterRentedOutAlbums(albums) {
        return albums.filter(album => album.rentedOut)
    }

    filterInStoreAlbums(albums) {
        return albums.filter(album => !album.rentedOut)
    }

    renderShelves() {

        let albums = this.props.albums
        if(albums.json !== undefined) {
            albums = JSON.parse(albums.json);
        }
        return <div>
            <h3>Rented Out</h3>
            <RentedOut albums={this.filterRentedOutAlbums(albums)}/>
            <h3>In Store</h3>
            <InStore albums={this.filterInStoreAlbums(albums)}/>
        </div>
    }

    render() {
        return (
            this.ifShowSpinner() ? <CircularProgress className={this.props.classes.progress} /> : this.renderShelves()
        );
    }
}

const mapDispatchToProps = dispatch => ({
  loadInitialState: (json) => dispatch({ type: EVENT.LOAD_INITIAL_STATE, data: { json } }),
});

const mapStateToProps = state => ({
    albums: state.appReducer.albums
})

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Home));
